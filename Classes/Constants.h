//
//  Constants.h
//  SushiNeko
//
//  Created by BYOF Studios on 05/01/16.
//
//

#ifndef Constants_h
#define Constants_h


enum class Side
{
    Left,
    Right,
    None
};


#endif /* Constants_h */
