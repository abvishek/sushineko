//
//  Character.cpp
//  SushiNeko
//
//  Created by BYOF Studios on 05/01/16.
//
//

#include "Character.h"
using namespace cocos2d;


Side Character::getSide()
{
    return this->side;
}

void Character::setSide(Side side)
{
    this->side = side;
    if (this->side == Side::Right)
    {
        this->setScaleX(-1.0f);
    }
    else
    {
        this->setScaleX(1.0f);
    }
}

