#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Piece.h"
#include "Constants.h"
#include "ui/CocosGUI.h"

enum class GameState
{
    Playing,
    GameOver
};


class Character;

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    Side getSideForObstacle(Side lastSide);
   
    void stepTower();
  

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
    
private:
    cocos2d::Node* pieceNode;
    cocos2d::Vector<Piece*> pieces;
    Character* character;
    Side lastObstacleSide;
    int pieceIndex;
    int score;
    float timeLeft;
    cocos2d::Sprite* timeBar;
    GameState gameState;
    cocos2d::ui::Text* scoreLabel;
    
    void onEnter() override;
    void update(float dt) override;
    void setupTouchHandling();
    bool isGameOver();
    void triggerGameOver();
    void resetGameState();
    void triggerPlaying();
    void setScore(int score);
    void setTimeLeft(float timeLeft);
};

#endif // __HELLOWORLD_SCENE_H__
