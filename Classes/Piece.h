#ifndef __SushiNeko__Piece__
#define __SushiNeko__Piece__

#include "cocos2d.h"
#include "Constants.h"

class Piece : public cocos2d::Node
{
public:
    
    float getSpriteHeight();
    CREATE_FUNC(Piece);
    void setObstacleSide(Side side);
    Side getObstacleSide();
    

protected:
    Side obstacleSide;
};

#endif /* defined(__SushiNeko__Piece__) */