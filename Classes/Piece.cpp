//
//  Piece.cpp
//  SushiNeko
//
//  Created by BYOF Studios on 05/01/16.
//
//

#include "Piece.h"
using namespace cocos2d;


float Piece::getSpriteHeight()
{
    // first grab a reference to the roll sprite
    Sprite* roll = this->getChildByName<Sprite*>("roll");
    
    // then return the roll sprite's height
    return roll->getContentSize().height;
}

Side Piece::getObstacleSide()
{
    return this->obstacleSide;
}

void Piece::setObstacleSide(Side side)
{
    this->obstacleSide = side;
    
    Sprite* roll = this->getChildByName<Sprite*>("roll");
    
    Sprite* leftChopstick = roll->getChildByName<Sprite*>("leftChopstick");
    Sprite* rightChopstick = roll->getChildByName<Sprite*>("rightChopstick");
    switch (this->obstacleSide)
    {
        case Side::None:
            break;
            
        case Side::Left:
            leftChopstick->setVisible(true);
            rightChopstick->setVisible(false);
            break;
            
        case Side::Right:
            leftChopstick->setVisible(false);
            rightChopstick->setVisible(true);
            break;
    }
}