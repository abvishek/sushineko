<GameFile>
  <PropertyGroup Name="MainScene" Type="Scene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="2.3.3.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="1080.0000" Y="1920.0000" />
        <Children>
          <AbstractNodeData Name="background_1" ActionTag="-1122654561" Tag="12" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="156.0000" RightMargin="156.0000" TopMargin="784.0000" ctype="SpriteObjectData">
            <Size X="768.0000" Y="1136.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="540.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="0.7111" Y="0.5917" />
            <FileData Type="Normal" Path="Assets/Images/resources-2x/background.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="character" ActionTag="2132717573" Tag="14" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="540.0000" RightMargin="540.0000" TopMargin="1690.0000" BottomMargin="230.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="540.0000" Y="230.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1198" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Character.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Piece" ActionTag="-1026807366" Tag="17" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="540.0000" RightMargin="540.0000" TopMargin="1628.0000" BottomMargin="292.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="540.0000" Y="292.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1521" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Piece.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="pieceNode" ActionTag="1424264834" Tag="22" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="540.0000" RightMargin="540.0000" TopMargin="1514.0000" BottomMargin="406.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="540.0000" Y="406.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.2115" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="scoreLabel" ActionTag="-1992768209" Tag="23" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="513.5000" RightMargin="513.5000" TopMargin="614.5000" BottomMargin="1190.5000" FontSize="100" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="53.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="540.0000" Y="1248.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6500" />
            <PreSize X="0.0491" Y="0.0599" />
            <FontResource Type="Normal" Path="Assets/Fonts/Game of Three.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="play" ActionTag="-341094310" VisibleForFrame="False" Tag="24" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="438.5000" RightMargin="438.5000" TopMargin="897.0000" BottomMargin="897.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="173" Scale9Height="104" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="203.0000" Y="126.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="540.0000" Y="960.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.1880" Y="0.0656" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Assets/Images/resources-2x/buttonPressed.png" Plist="" />
            <NormalFileData Type="Normal" Path="Assets/Images/resources-2x/button.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="lifeBG" ActionTag="-67899746" Tag="25" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="386.5000" RightMargin="386.5000" TopMargin="38.4000" BottomMargin="1799.6000" ctype="SpriteObjectData">
            <Size X="307.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="lifeBar" ActionTag="-615494105" Tag="26" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="17.0000" RightMargin="18.0000" TopMargin="16.5000" BottomMargin="16.5000" ctype="SpriteObjectData">
                <Size X="272.0000" Y="49.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="17.0000" Y="41.0000" />
                <Scale ScaleX="0.5000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0554" Y="0.5000" />
                <PreSize X="0.8860" Y="0.5976" />
                <FileData Type="Normal" Path="Assets/Images/resources-2x/life.png" Plist="" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="540.0000" Y="1881.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9800" />
            <PreSize X="0.2843" Y="0.0427" />
            <FileData Type="Normal" Path="Assets/Images/resources-2x/life_bg.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>